## 1.1.6

### New
- Update to support Spigot 1.15
- Update bStats
- Update AWS s3 SDK

## 1.1.5

### Bugfixes
- Relocate `org.apache` to resolve dependency conflicts.

## 1.1.4

### Bugfixes
- Check for ongoing backups before initiating a new one

## 1.1.3

### Bugfixes
- Ordering list output

### New
- Adding disk usage of backups to list command

## 1.1.2

### New
- Adding user variable for signed URL duration

## 1.1.1

### Bugfixes
- Fix issue where a blank prefix would result in the regex check failing

### New
- Add support for third party s3 services

## 1.1.0

### New
- Update to support Spigot 1.14.4

## 1.0.1

### New
- Update to support Spigot 1.14.1

## 1.0.0

### New
- Initial release